# Système d'Authentification et d'Accès aux Ressources

Ce projet implémente un système d'authentification basé sur la génération de codes aléatoires de 6 chiffres, ainsi qu'un serveur de ressources qui vérifie l'authenticité des codes générés.

## Fonctionnalités

- Le serveur d'authentification génère des codes aléatoires de 6 chiffres et les associe à des identifiants d'utilisateurs.
- Chaque code a une durée de validité de 30 secondes.
- Le serveur de ressources vérifie l'authenticité des codes envoyés par les utilisateurs.

## Arborescence

```
.
├── docker-compose.yml
├── srv-aut
│   ├── Dockerfile
│   ├── main.go
│   └── go.mod
└── srv-src
    ├── Dockerfile
    ├── go.mod
    ├── main.go
    └── templates
        └── index.html
```

## Prérequis

- Docker installé sur votre système.

## Installation et Utilisation

1. Clonez ce dépôt sur votre machine locale :

    ```bash
    git clone https://gitlab.com/V-a-v-a-n/go-2fa
    ```

2. Accédez au répertoire racine du projet :

    ```bash
    cd go-2fa
    ```

3. Démarrez les services à l'aide de Docker Compose :

    ```bash
    docker-compose up --build
    ```

4. Une fois que les conteneurs sont en cours d'exécution, vous pouvez accéder au serveur d'authentification à l'adresse `http://localhost:8080` et au serveur de ressources à l'adresse `http://localhost:9090`.

## Utilisation

### Serveur d'Authentification

- Pour générer un code pour un utilisateur spécifique, envoyez une requête GET à l'endpoint `/generate-code` avec le paramètre `userID` spécifiant l'identifiant de l'utilisateur.

### Serveur de Ressources

- Pour vérifier un code d'authentification, envoyez une requête GET à l'endpoint `/verify-code` avec le code d'authentification dans l'en-tête `X-Auth-Code`.

## Auteur

Réalisé par [Evan Vettier]