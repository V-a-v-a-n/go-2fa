package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type CodeData struct {
	Code       string    `json:"code"`
	Expiration time.Time `json:"expiration"`
}

var (
	codes      = make(map[string]CodeData)
	codesMutex sync.Mutex
)

func generateCode() string {
	rand.Seed(time.Now().UnixNano())
	return fmt.Sprintf("%06d", rand.Intn(1000000))
}

func generateAndStoreCode(userID string) string {
	code := generateCode()
	expiration := time.Now().Add(30 * time.Second)

	codesMutex.Lock()
	defer codesMutex.Unlock()
	codes[userID] = CodeData{Code: code, Expiration: expiration}

	return code
}

func getCode(userID string) (string, bool) {
	codesMutex.Lock()
	defer codesMutex.Unlock()

	codeData, exists := codes[userID]
	if !exists || time.Now().After(codeData.Expiration) {
		return "", false
	}

	return codeData.Code, true
}

func deleteCode(userID string) {
	codesMutex.Lock()
	defer codesMutex.Unlock()
	delete(codes, userID)
}

func handleGenerateCode(w http.ResponseWriter, r *http.Request) {
	userID := r.URL.Query().Get("userID")
	if userID == "" {
		http.Error(w, "UserID is required", http.StatusBadRequest)
		return
	}

	code := generateAndStoreCode(userID)
	resp := map[string]string{"code": code}
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResp)
}

func main() {
	http.HandleFunc("/generate-code", handleGenerateCode)

	fmt.Println("Server d'authentification démarré sur le port :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
