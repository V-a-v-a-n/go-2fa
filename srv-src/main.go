package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

type AuthResponse struct {
	Status string `json:"status"`
}

func verifyCode(code string) bool {
	// Placeholder function for code verification logic
	// For this example, just check if the code is not empty
	return code != ""
}

func handleVerifyCode(w http.ResponseWriter, r *http.Request) {
	code := r.Header.Get("X-Auth-Code")
	if code == "" {
		http.Error(w, "Auth Code is required", http.StatusBadRequest)
		return
	}

	// Simulate verification process
	if !verifyCode(code) {
		resp := AuthResponse{Status: "401 Unauthorized"}
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(jsonResp)
		return
	}

	resp := AuthResponse{Status: "200 OK"}
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonResp)
}

func main() {
	http.HandleFunc("/verify-code", handleVerifyCode)

	fmt.Println("Serveur de ressources démarré sur le port :9090")
	log.Fatal(http.ListenAndServe(":9090", nil))
}
